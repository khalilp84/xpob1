﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Xpo;

namespace XpoB1.Models
{
    public class MyTableDocComment : XPObject
    {
        public MyTableDocComment(Session session)
            :base(session)
        {
            
        }

        private int fDocEntry;
        public int DocEntry
        {
            get { return fDocEntry; }
            set { SetPropertyValue("DocEntry", ref fDocEntry, value); }
        }

        private string fComments;
        public string Comments
        {
            get { return fComments; }
            set { SetPropertyValue("Comments", ref fComments, value); }
        }

    }
}
