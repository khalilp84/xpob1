﻿using DevExpress.Xpo;
using System;
using System.Data;

namespace XpoB1.DI
{
    public class SqlHelper
    {
        public static int GetVersaoSqlServer(string companyDb)
        {
            try
            {
                const string sql = @"SELECT SERVERPROPERTY('productversion')";

                var session = ConnectionHelper.GetSession(companyDb);
                var dt = ExecutaSelect(sql, session);

                var productionServer = dt.Rows[0][0].ToString();
                var result = productionServer.Substring(0, 2);

                switch (result)
                {
                    case "12":
                        return 2014;
                    case "11":
                        return 2012;
                    case "10":
                        return 2008;
                    default:
                        return 2005;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private static DataTable ExecutaSelect(string sql, Session session)
        {
            try
            {
                session = session ?? Session.DefaultSession;
                var cmd = session.Connection.CreateCommand();
                cmd.CommandText = sql;
                cmd.CommandTimeout = TimeSpan.FromMinutes(15).Seconds;
                var reader = cmd.ExecuteReader();
                var dt = new DataTable();
                dt.Load(reader);

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
