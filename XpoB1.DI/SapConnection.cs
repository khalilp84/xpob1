﻿using SAPbobsCOM;
using System;

namespace XpoB1.DI
{
    public class SapConnection
    {
        private static Company _conSap;

        public static Company Instance
        {
            get
            {
                if (_conSap == null)
                    throw new Exception("Conexão SAP não efetuada.");

                return _conSap;
            }
        }

        public static Company GetSapConnection(string pServidor, string pDbSap, string pServidorLicenca
            , string pUsuarioDb, string pSenhaDb, string pUsuarioSap, string pSenhaSap)
        {
            _conSap = new Company
            {
                Server = pServidor,
                CompanyDB = pDbSap,
                LicenseServer = pServidorLicenca,
                DbUserName = pUsuarioDb,
                DbPassword = pSenhaDb,
                DbServerType = GetDbServerType(pDbSap),
                UseTrusted = false,
                UserName = pUsuarioSap,
                Password = pSenhaSap,
                language = BoSuppLangs.ln_Portuguese_Br
            };

            return _conSap;
        }

        private static BoDataServerTypes GetDbServerType(string companyDb)
        {
            var versaoSql = SqlHelper.GetVersaoSqlServer(companyDb);
            switch (versaoSql)
            {
                case 2014:
                    return BoDataServerTypes.dst_MSSQL2014;
                case 2012:
                    return BoDataServerTypes.dst_MSSQL2012;
                case 2008:
                    return BoDataServerTypes.dst_MSSQL2008;
                default:
                    return BoDataServerTypes.dst_MSSQL2005;

            }
        }
    }
}
