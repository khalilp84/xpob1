﻿using DevExpress.Xpo;
using System;

namespace XpoB1.DI.Tables
{
    [Persistent("OBIN")]
    public class OBIN
    {
        [Key]
        public int AbsEntry { get; set; }
        public string BinCode { get; set; }
        public string WhsCode { get; set; }
        public string Descr { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
