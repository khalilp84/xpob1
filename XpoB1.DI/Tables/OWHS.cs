﻿using DevExpress.Xpo;

namespace XpoB1.DI.Tables
{
    [Persistent("OWHS")]
    public class OWHS
    {
        [Key]
        public string WhsCode { get; set; }
        public string WhsName { get; set; }
    }
}
