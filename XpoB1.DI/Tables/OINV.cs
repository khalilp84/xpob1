﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XpoB1.DI.Tables
{
    [Persistent("OINV")]
    public class OINV
    {
        [Key]
        public int DocEntry { get; set; }

        public DateTime DocDate { get; set; }
        public decimal DocTotal { get; set; }        
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public int SlpCode { get; set; }

        public string Comments { get; set; }

    }
}
