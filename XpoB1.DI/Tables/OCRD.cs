﻿using DevExpress.Xpo;

namespace XpoB1.DI.Tables
{
    [Persistent("OCRD")]
    public class OCRD
    {

        [Key]
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public string CardFName { get; set; }
        public string ZipCode { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }

    }
}
