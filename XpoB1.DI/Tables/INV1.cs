﻿using DevExpress.Xpo;

namespace XpoB1.DI.Tables
{
    [Persistent("INV1")]
    public class INV1
    {
        [Key]
        public int DocEntry { get; set; }
        public int LineNum { get; set; }
        public string ItemCode { get; set; }
        public string Dscription { get; set; }
        public decimal Price { get; set; }
    }
}
