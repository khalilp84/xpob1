﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XpoB1.DI.Tables;

namespace XpoB1.DI.Repositories
{
    public class DocumentRepository
    {
        private string _companyDb;

        public DocumentRepository(string companyDb)
        {
            _companyDb = companyDb;
        }

        public List<OINV> GetBy(DateTime startDate, DateTime endDate)
        {
            using (var session = ConnectionHelper.GetSession(_companyDb))
            {
                return new XPQuery<OINV>(session)
                      .Where(v => v.DocDate >= startDate &&
                                  v.DocDate <= endDate)
                      .ToList();
            }
        }

        public List<OINV> GetByDescendingDocDate(int topN)
        {
            using (var session = ConnectionHelper.GetSession(_companyDb))
            {
                return new XPQuery<OINV>(session)
                      .OrderByDescending(v => v.DocDate)
                      .Take(topN)
                      .ToList();
            }
        }

        public OINV GetByKey(object id)
        {
            using (var session = ConnectionHelper.GetSession(_companyDb))
            {
                return session.GetObjectByKey<OINV>(id);
            }
        }
    }
}
