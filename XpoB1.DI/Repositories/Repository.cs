﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace XpoB1.DI.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private string _companyDb;

        public Repository(string companyDb)
        {
            _companyDb = companyDb;
        }

        public IEnumerable<T> GetAll(Expression<Func<T, bool>> filter = null)
        {
            var list = new List<T>();

            using (var session = ConnectionHelper.GetSession(_companyDb))
            {
                if (filter == null)
                    list.AddRange(new XPQuery<T>(session));
                else
                    list.AddRange(new XPQuery<T>(session).Where(filter));
            }

            return list;

        }

        public T GetByKey(object id)
        {
            T obj;

            using (var session = ConnectionHelper.GetSession(_companyDb))
            {
                obj = session.GetObjectByKey<T>(id);
            }

            return obj;
        }
        
    }
}
