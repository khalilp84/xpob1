﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XpoB1.DI.Tables;

namespace XpoB1.DI.Repositories
{
    public class BusinessPartnerRepository
    {
        private string _companyDb;

        public BusinessPartnerRepository(string companyDb)
        {
            _companyDb = companyDb;
        }

        public List<OCRD> GetBy(int topN)
        {
            using (var session = ConnectionHelper.GetSession(_companyDb))
            {
                return new XPQuery<OCRD>(session)
                  .Take(topN)
                  .ToList();
            }
        }

        public OCRD GetByKey(string cardCode)
        {
            using (var session = ConnectionHelper.GetSession(_companyDb))
            {
                return session.GetObjectByKey<OCRD>(cardCode);
            }
        }
    }
}
