﻿using DevExpress.Xpo;
using DevExpress.XtraEditors;
using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using XpoB1.DI;
using XpoB1.DI.Repositories;
using XpoB1.DI.Tables;
using XpoB1.Models;

namespace XpoB1
{
    public partial class FormAtualizaDocB1 : XtraForm
    {
        public FormAtualizaDocB1()
        {
            InitializeComponent();

            FormataComboDocumento();
        }

        private void FormataComboDocumento()
        {
            try
            {
                CarregaDocumentosCombo();

                documentoComboBox.Properties.DisplayMember = "DocEntry";
                documentoComboBox.Properties.ValueMember = "DocEntry";

                documentoComboBox.EditValueChanged += documentoComboBox_EditValueChanged;
            }
            catch (Exception ex)
            {
                Util.ShowMessageError(ex.Message);
            }
        }

        void documentoComboBox_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                var doc = GetSelected();

                memoEdit1.Text = doc.Comments;

            }
            catch (Exception ex)
            {
                Util.ShowMessageError(ex.Message);
            }
        }

        private OINV GetSelected()
        {
            var currentDocEntry = documentoComboBox.EditValue;

            if (currentDocEntry == null)
                throw new Exception("Nenhum documento selecionado.");

            return new DocumentRepository(DemoConsts.CompanyDb).GetByKey(currentDocEntry);
        }

        private void CarregaDocumentosCombo()
        {
            var repositorio = new DocumentRepository(DemoConsts.CompanyDb);
            var documents = repositorio.GetByDescendingDocDate(10);

            documentoComboBox.Properties.DataSource = null;
            documentoComboBox.Properties.DataSource = documents;
        }

        private void ConectaNoSap()
        {
            try
            {
                var conSap = SapConnection.GetSapConnection(
                    DemoConsts.Server,
                    DemoConsts.CompanyDb,
                    DemoConsts.LicenseServer,
                    DemoConsts.UserDb,
                    DemoConsts.PasswordDb,
                    DemoConsts.UserSap,
                    DemoConsts.PasswordSap
                    );

                Cursor.Current = Cursors.WaitCursor;

                if (conSap.Connect() != 0)
                {
                    var errorMsg = "Erro ao efetuar conexão com o SAP: " + conSap.GetLastErrorDescription();
                    throw new Exception(errorMsg);
                }
                else
                {
                    Util.ShowMessageSuccess("Conexão com o SAP realizada.");
                    btnConectaSap.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Util.ShowMessageError(ex.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                ConectaNoSap();
            }
            catch (Exception ex)
            {
                Util.ShowMessageError(ex.Message);
            }
        }

        private void btnAtualizaDocB1_Click(object sender, EventArgs e)
        {
            try
            {
                ValidaConexaoSap();

                AtualizaDadosB1();

            }
            catch (Exception ex)
            {
                Util.ShowMessageError(ex.Message);
            }
        }

        private void AtualizaDadosB1()
        {
            var currentDoc = GetSelected();
            var newComment = memoEdit1.Text;

            using (Session session = new Session())
            {
                session.BeginTransaction();

                try
                {
                    var myComments = Session.DefaultSession.Query<MyTableDocComment>()
                                .Where(v => v.DocEntry == currentDoc.DocEntry)
                                .FirstOrDefault()
                                ?? new MyTableDocComment(Session.DefaultSession);

                    myComments.DocEntry = currentDoc.DocEntry;
                    myComments.Comments = newComment;

                    myComments.Save();

                    var conSap = SapConnection.Instance;

                    var doc = conSap.GetBusinessObject(BoObjectTypes.oInvoices) as Documents;
                    if (doc.GetByKey(currentDoc.DocEntry))
                    {
                        doc.Comments = newComment;
                    }

                    if (doc.Update() != 0)
                    {
                        throw new Exception("erro ao atualizar documento no b1");
                    }

                    session.CommitTransaction();
                    //session.RollbackTransaction();
                }
                catch
                {
                    session.RollbackTransaction();
                }
            }

            Util.ShowMessageSuccess();

        }

        private void ValidaConexaoSap()
        {
            if (btnConectaSap.Enabled)
                throw new Exception("Favor conectar no SAP");
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            try
            {
                ValidaConexaoSap();

                AdicionaTransfEstoque();
            }
            catch (Exception ex)
            {
                Util.ShowMessageError(ex.Message);
            }
        }

        private void AdicionaTransfEstoque()
        {
            var doc = GetSboTransferDocument();

            var date = DateTime.Now;
            doc.DocDate = date;
            doc.DueDate = date;
            doc.TaxDate = date;

            doc.Comments = string.Format("Transferência de acordo com o pedido {0} feito no wms.", 999);

            // LINES
            doc.Lines.SetCurrentLine(0);
            doc.Lines.ItemCode = "P000193";
            doc.Lines.FromWarehouseCode = "01";
            doc.Lines.WarehouseCode = "01";

            var quantity = 1;

            doc.Lines.Quantity = quantity;

            // BATCH NUMBERS
            doc.Lines.BatchNumbers.SetCurrentLine(0);
            doc.Lines.BatchNumbers.BatchNumber = "B15D0947";
            doc.Lines.BatchNumbers.Quantity = quantity;
            doc.Lines.BatchNumbers.Add();

            // BIN LOCATIONS
            // from ( de )
            doc.Lines.BinAllocations.SetCurrentLine(0);
            doc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = 0;
            doc.Lines.BinAllocations.BinActionType = BinActionTypeEnum.batFromWarehouse;
            doc.Lines.BinAllocations.BinAbsEntry = 2; // 01-01
            doc.Lines.BinAllocations.Quantity = quantity;
            doc.Lines.BinAllocations.Add();

            // to ( para )
            doc.Lines.BinAllocations.SetCurrentLine(1);
            doc.Lines.BinAllocations.SerialAndBatchNumbersBaseLine = 0;
            doc.Lines.BinAllocations.BinActionType = BinActionTypeEnum.batToWarehouse;
            doc.Lines.BinAllocations.BinAbsEntry = 3; // 01-02
            doc.Lines.BinAllocations.Quantity = quantity;
            doc.Lines.BinAllocations.Add();

            string error = string.Empty;
            int docEntry = 0;

            var returnCode = doc.Add();

            if (returnCode != 0)
            {
                error = SapConnection.Instance.GetLastErrorDescription();
            }
            else
            {
                int.TryParse(SapConnection.Instance.GetNewObjectKey(), out docEntry);
                var msg = string.Format("Trans. com o docentry [{0}] gerada com sucesso !", docEntry);
                Util.ShowMessageSuccess(msg);
            }

        }

        private StockTransfer GetSboTransferDocument()
        {
            var docObjType = BoObjectTypes.oStockTransfer;

            var conSap = SapConnection.Instance;
            return conSap.GetBusinessObject((BoObjectTypes)docObjType) as SAPbobsCOM.StockTransfer;
        }
    }
}
;