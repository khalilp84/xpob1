﻿namespace XpoB1
{
    partial class FormAtualizaDocB1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.documentoComboBox = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnConectaSap = new DevExpress.XtraEditors.SimpleButton();
            this.btnAtualizaDocB1 = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.documentoComboBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // documentoComboBox
            // 
            this.documentoComboBox.Location = new System.Drawing.Point(93, 16);
            this.documentoComboBox.Name = "documentoComboBox";
            this.documentoComboBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.documentoComboBox.Properties.NullText = "[Selecione um documento]";
            this.documentoComboBox.Size = new System.Drawing.Size(151, 20);
            this.documentoComboBox.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 19);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(54, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Documento";
            // 
            // btnConectaSap
            // 
            this.btnConectaSap.Location = new System.Drawing.Point(440, 19);
            this.btnConectaSap.Name = "btnConectaSap";
            this.btnConectaSap.Size = new System.Drawing.Size(90, 30);
            this.btnConectaSap.TabIndex = 2;
            this.btnConectaSap.Text = "Conectar no SAP";
            this.btnConectaSap.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btnAtualizaDocB1
            // 
            this.btnAtualizaDocB1.Location = new System.Drawing.Point(440, 70);
            this.btnAtualizaDocB1.Name = "btnAtualizaDocB1";
            this.btnAtualizaDocB1.Size = new System.Drawing.Size(90, 30);
            this.btnAtualizaDocB1.TabIndex = 3;
            this.btnAtualizaDocB1.Text = "Atualiza Doc B1";
            this.btnAtualizaDocB1.Click += new System.EventHandler(this.btnAtualizaDocB1_Click);
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(93, 56);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(304, 96);
            this.memoEdit1.TabIndex = 4;
            this.memoEdit1.UseOptimizedRendering = true;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 58);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(50, 13);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "Comments";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(440, 122);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(90, 30);
            this.simpleButton1.TabIndex = 6;
            this.simpleButton1.Text = "Trans. de Estoque";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // FormAtualizaDocB1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 183);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.memoEdit1);
            this.Controls.Add(this.btnAtualizaDocB1);
            this.Controls.Add(this.btnConectaSap);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.documentoComboBox);
            this.Name = "FormAtualizaDocB1";
            this.Text = "Atualiza documento do B1";
            ((System.ComponentModel.ISupportInitialize)(this.documentoComboBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit documentoComboBox;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnConectaSap;
        private DevExpress.XtraEditors.SimpleButton btnAtualizaDocB1;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}