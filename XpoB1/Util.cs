﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XpoB1
{
    public static class Util
    {
        public static void ShowMessageError(string pTexto)
        {
            XtraMessageBox.Show(pTexto, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ShowMessageInformation(string pTexto)
        {
            XtraMessageBox.Show(pTexto, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void ShowMessageSuccess(string pTexto = "Dados salvos com sucesso.")
        {
            XtraMessageBox.Show(pTexto, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void ShowMessageWarning(string pTexto, string pCaption = "Aviso")
        {
            XtraMessageBox.Show(pTexto, pCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
