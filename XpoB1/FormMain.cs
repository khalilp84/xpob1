﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XpoB1
{
    public partial class FormMain : XtraForm
    {
        public FormMain()
        {
            InitializeComponent();
        }

        public void AbreForm<T>() where T : XtraForm, new()
        {
            try
            {
                var form = new T();
                form.Icon = this.Icon;
                form.StartPosition = FormStartPosition.CenterScreen;
                form.Show();
            }
            catch (Exception ex)
            {
                Util.ShowMessageError(ex.Message);
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            AbreForm<Form1>();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            AbreForm<FormAtualizaDocB1>();
        }
    }
}
