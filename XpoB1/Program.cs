﻿using DevExpress.Xpo.DB;
using System;
using System.Windows.Forms;
using XpoB1.DI;

namespace XpoB1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            InitDAL();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain());
        }

        private static void InitDAL()
        {
            const string Server = "LEANDROKHALIL";
            const string UserDb = "sa";
            const string UserDbPassword = "sap@123";

            var sqlConn = ConnectionHelper.GetSqlServerConnectionString(
                DemoConsts.Server, DemoConsts.UserDb, DemoConsts.PasswordDb, DemoConsts.MyDatabase);

            ConnectionHelper.Connect(sqlConn, AutoCreateOption.DatabaseAndSchema);
        }
    }
}
