﻿using DevExpress.Xpo;
using DevExpress.XtraEditors;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using XpoB1.DI;
using XpoB1.DI.Repositories;
using XpoB1.DI.Tables;

namespace XpoB1
{
    public partial class Form1 : XtraForm
    {
        public Form1()
        {
            InitializeComponent();

            dataInicioEdit.DateTime = new DateTime(2015, 3, 1);
            dataFimEdit.DateTime = new DateTime(2015, 5, 1);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                gridControl1.DataSource = null;

                var repositorio = new DocumentRepository(DemoConsts.CompanyDb);
                var documentList = repositorio.GetBy(dataInicioEdit.DateTime, dataFimEdit.DateTime);

                lblCount.Text = documentList.Count().ToString();
                gridControl1.DataSource = documentList;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            try
            {
                gridControl1.DataSource = null;

                var repositorio = new BusinessPartnerRepository(DemoConsts.CompanyDb);
                var partners = repositorio.GetBy(10);

                lblCount.Text = partners.Count().ToString();
                gridControl1.DataSource = partners;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
