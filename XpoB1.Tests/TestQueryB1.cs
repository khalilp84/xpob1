﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XpoB1.DI;
using XpoB1.DI.Repositories;
using Xunit;

namespace XpoB1.Tests
{
    public class TestQueryB1
    {
        const string _companyDb = "SBODEMOBR";

        private string GetConnectionString()
        {
            return ConnectionHelper.GetSqlServerConnectionString(
                "LEANDROKHALIL", "sa", "sap@123", _companyDb);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(10)]
        public void TESTA_CONSULTA_TOP10_PARCEIRO_DE_NEGOCIOS(int topN)
        {
            ConnectionHelper.Connect(GetConnectionString());

            var repositorio = new BusinessPartnerRepository(_companyDb);
            var partners = repositorio.GetBy(topN);

            Assert.Equal(topN, partners.Count);
        }

        [Fact]
        public void TESTE_GET_BY_CARDCODE()
        {
            ConnectionHelper.Connect(GetConnectionString());

            var repositorio = new BusinessPartnerRepository(_companyDb);
            var parceiro = repositorio.GetByKey("C20000");

            Assert.False(parceiro == null);
            Assert.Equal("Maxi-Teq do Brasil LTDA", parceiro.CardName);
            Assert.Equal("01489-900", parceiro.ZipCode);
        }
    }
}
