﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XpoB1.DI;
using XpoB1.DI.Repositories;
using XpoB1.DI.Tables;
using Xunit;

namespace XpoB1.Tests
{
    public class DefaultRepositoryTest
    {
        const string _companyDb = "SBOMW";

        private string GetConnectionString()
        {
            return ConnectionHelper.GetSqlServerConnectionString(
                "LEANDROKHALIL", "sa", "sap@123", _companyDb);
        }

        [Fact]
        public void TESTE_GET_ALL_BILOCATIONS()
        {
            ConnectionHelper.Connect(GetConnectionString());

            var repositorio = new Repository<OBIN>(_companyDb);
            var binLocations = repositorio.GetAll();

            Assert.True(binLocations.Any());
        }

        [Fact]
        public void TESTE_GET_ALL_BINLOCATIONS_WHERE_WHSCODE()
        {
            ConnectionHelper.Connect(GetConnectionString());

            var repositorio = new Repository<OBIN>(_companyDb);
            var binLocations = repositorio.GetAll(v => v.WhsCode == "MW01");

            Assert.True(binLocations.Any());
        }

        [Fact]
        public void TESTE_GET_ALL_WAREHOUSES()
        {
            ConnectionHelper.Connect(GetConnectionString());

            var repositorio = new Repository<OWHS>(_companyDb);
            var warehouses = repositorio.GetAll();

            Assert.True(warehouses.Any());
        }
    }
}
